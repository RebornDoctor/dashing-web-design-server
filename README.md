# Strapi Server README.md

## Introduction

This README provides instructions for setting up and running the Strapi server

## Setup and Run the Strapi Server

1. **Clone the server repository**

    ```bash
    git clone [Server Repository URL]
    ```

2. **Switch to the main branch**

    ```bash
    cd [Server Repository Directory]
    git checkout main
    ```

3. **Install dependencies and run the Strapi server**

    ```bash
    npm install
    npm run strapi develop
    ```
    
The Strapi CMS should now be running at [http://localhost:1337/](http://localhost:1337/).

## Strapi CMS Credentials
- **URL:** [http://localhost:1337/](http://localhost:1337/)
- **Email:** test@email.com
- **Password:** vmznE+irStvPS-8

